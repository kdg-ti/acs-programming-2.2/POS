#POS  process sale

This is an implementaton of the **process sale** use case in the POS business case in
Applying UML and patterns (Larman), based on a 3-layer architecture.
Note: do not switch from branches using gradle to branches not using gradle, make a fresh clone for branches not using gradle.

## branches

### master

Base case using
- JUnit 5 tests
- Two possible persistence mechanisms
  - memory (hashmaps) with GenericMemory implementation
  - Apache derby in memory database
- application.properties is used to choose a persistence mechanism
-  stateful controller


### SpringDI
branch of dependencyInverted
- based on Spring and Gradle

### SpringCucumber
branch of dependencyInverted
- based on Spring and Gradle with cucumbertests

## old branches

### DependencyInverted
older branch of master
- does NOT use gradle

- JUnit 5 tests
- Two possible persistence mechanisms
  - memory (hashmaps) with GenericMemory implementation
  - Apache derby in memory database
- Business classes use memory repository


### statelessController
branch of DependencyInverted
- does NOT use gradle
- stateless controller
  - alle external communication based on id's

### statelessControllerRepoFactories
- does NOT use gradle
- branch of statelessController, factory for repositories added



### SpringDB
branch of springDI using DB
work in progress




### runCucumberFromGradle
obsolete branch of dependencyInverted
- based on Spring with cucumbertests that can be started from Gradle