package demo.pos.persistence;

import demo.persistence.Repository;
import demo.pos.domain.sale.Sale;

/**
 * This interface and its implementations are not needed
 * You might as well use an object of type Repository<Long,Sale>
 */
public interface SaleRepository extends Repository<Long,Sale>{
}
