package demo.pos.persistence.db;

import demo.persistence.db.DbConnection;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;
import demo.pos.domain.sale.Payment;
import demo.pos.persistence.ProductDescriptionRepository;

import java.sql.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by overvelj on 14/11/2016.
 */
@SuppressWarnings("SqlResolve")
public class ProductDescriptionDbRepository implements ProductDescriptionRepository {

	private static Logger logger = Logger.getLogger("demo.pos.persistence.db.ProductDbCatalog");

	// all product description prices are saved with the default local currency
	private static final String CREATE_TABLE = "CREATE TABLE product_descriptions(\n"
		+ "\t item_id INTEGER PRIMARY KEY,\n"
		+ "\t description VARCHAR(50),\n"
		+ "\t price NUMERIC (20,2)"
		+ ")";

	private final String INSERT = "INSERT INTO product_descriptions(item_id,description, price) VALUES(?,?,?)";
	private final String UPDATE = "UPDATE  product_descriptions " +
			"set description = ?, price = ?,locale = ? " +
			"where item_id = ?";
	private final String SELECT_BY_ID = "SELECT * FROM product_descriptions " +
			"WHERE item_id = ?";
	private final String SELECT_BY_MAX_PRICE = "SELECT * FROM product_descriptions " +
			"WHERE price <= ?";
	private final String COUNT = "SELECT COUNT(*) FROM product_descriptions";


	public ProductDescriptionDbRepository() {
		try (Statement stmt = DbConnection.getInstance().createStatement()) {
			stmt.executeUpdate(CREATE_TABLE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ProductDescription insert(Long key, ProductDescription pd) {
		try (PreparedStatement stmt = DbConnection.getInstance()
				.prepareStatement(INSERT)) {
			stmt.setLong(1, key);
			stmt.setString(2, pd.getDescription());
			stmt.setDouble(3, pd.getPrice().getAmount());
			int result = stmt.executeUpdate();
			if (result == 1) {
				pd.setId(key);
				return pd;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean update(Long key, ProductDescription value) {
		try (PreparedStatement stmt = DbConnection.getInstance()
				.prepareStatement(UPDATE)) {
			stmt.setString(1, value.getDescription());
			stmt.setDouble(2, value.getPrice().getAmount());
			stmt.setLong(4, key);
			int result = stmt.executeUpdate();
			return result == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;	}

	@Override
	public ProductDescription findById(Long id) {
		try (PreparedStatement stmt = DbConnection.getInstance()
				.prepareStatement(SELECT_BY_ID)) {
			stmt.setLong(1, id);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {

				return new ProductDescription(
						result.getLong("item_id"),
						result.getString("description"),
						new Money(result.getDouble("price")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;	}

	@Override
	public long count() {
		try (PreparedStatement stmt = DbConnection.getInstance()
			.prepareStatement(COUNT)) {
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				return result.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Collection<ProductDescription> findWithMaxPrice(Money budget) {
		Collection<ProductDescription> products = new ArrayList<>();
		try (PreparedStatement stmt = DbConnection.getInstance()
				.prepareStatement(SELECT_BY_MAX_PRICE)) {
			stmt.setDouble(1, budget.getAmount());
			ResultSet result = stmt.executeQuery();
			while (result.next()) {

				products.add(new ProductDescription(
						result.getLong("item_id"),
						result.getString("description"),
						new Money(result.getDouble("amount"))));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}
}
