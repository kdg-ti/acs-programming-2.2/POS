package demo.pos.persistence.db;

import demo.persistence.db.DbConnection;
import demo.persistence.db.DbUtils;
import demo.pos.domain.common.Money;
import demo.pos.domain.sale.Payment;
import demo.pos.domain.sale.Sale;
import demo.pos.domain.sale.SalesLineItem;
import demo.pos.persistence.ProductDescriptionRepository;
import demo.pos.persistence.SaleRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by jan on 15/12/2016.
 */
@SuppressWarnings("SqlResolve")
public class SaleDbRepository implements SaleRepository {

	private static Logger logger = Logger.getLogger("demo.pos.persistence.db.ProductDbCatalog");
	private ProductDescriptionDbRepository productRepo;
	private final String[] CREATE_TABLE = {"""
		CREATE TABLE sales(
			id INTEGER PRIMARY KEY,
			is_complete NUMERIC(1),
			amount_payed NUMERIC(20,2),
			locale VARCHAR(7)
	)
	""","""
	CREATE TABLE sales_line_items(
		sales_id INTEGER REFERENCES sales(id)  ,
		sli_id INTEGER  ,
		qty INTEGER,
		prod_id INTEGER references product_descriptions(item_id),
		CONSTRAINT sli_pk PRIMARY KEY (sales_id,sli_id)
	)
	"""
	};

	private final String INSERT = "INSERT INTO sales VALUES(?,?,?,?)";
	private final String INSERT_SALESLINE = "INSERT INTO sales_line_items VALUES(?,?,?,?)";
	private final String SELECT_BY_ID = """
  SELECT * FROM sales s
		left join sales_line_items sli on  s.id = sli.sales_id
		WHERE id = ?
		""";

	private final String COUNT = "SELECT COUNT(*) FROM sales";

	public SaleDbRepository(ProductDescriptionRepository productDescriptionRepository) {
		try (Statement stmt = DbConnection.getInstance().createStatement()) {
			for (String table : CREATE_TABLE) {
				stmt.executeUpdate(table);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	@Override
	public boolean update(Long key, Sale value) {
		return false;
	}

	@Override
	public Sale insert(Long key, Sale sale) {
		try (PreparedStatement stmt = DbConnection.getInstance()
			.prepareStatement(INSERT)) {
			stmt.setLong(1, key);
			stmt.setInt(2, DbUtils.convertToDbType(sale.isComplete()));
			stmt.setDouble(3, sale.getPayment().getSum().getAmount());
			stmt.setString(4, sale.getPayment().getSum().getLocale().toString());
			int result = stmt.executeUpdate();
			if (result != 1) {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try (PreparedStatement stmt = DbConnection.getInstance()
			.prepareStatement(INSERT_SALESLINE)) {
			for (int i = 0; i < sale.getSalesLineItems().size(); i++) {
				SalesLineItem line = sale.getSalesLineItems().get(i);
				stmt.setLong(1, sale.getId());
				stmt.setLong(2, i);
				stmt.setInt(3, line.getQty());
				stmt.setLong(4, line.getPd().getId());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sale;
	}

	@Override
	public Sale findById(Long id) {
		Sale sale = null;
		try (PreparedStatement stmt = DbConnection.getInstance()
			.prepareStatement(SELECT_BY_ID)) {
			stmt.setLong(1, id);
			ResultSet result = stmt.executeQuery();
			if (!result.next()) {
				return null;
			}
			sale = new Sale(
				result.getLong("id"),
				result.getBoolean("is_complete"));
			double price = result.getDouble("price");
			if (price > 0.001) {
				sale.makePayment(new Money(result.getDouble("price"), result.getString("locale")));
			}
			long sli_id = result.getLong("sli_id");
			List<SalesLineItem> slis;
			if (sli_id != 0) {
				slis = new ArrayList<>();
				do {
					slis.add(new SalesLineItem(
						result.getLong("sli_id"),
						productRepo.findById(result.getLong("prod_id")),
						result.getInt("qty"))
					);
				} while (result.next());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sale;
	}


	@Override
	public long count() {
		logger.info("counting sales in database");
		try (PreparedStatement stmt = DbConnection.getInstance()
			.prepareStatement(COUNT)) {
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				return result.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
