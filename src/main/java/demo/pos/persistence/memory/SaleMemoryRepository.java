package demo.pos.persistence.memory;

import demo.persistence.memory.MemoryRepository;
import demo.pos.domain.sale.Sale;
import demo.pos.persistence.SaleRepository;

import java.util.*;
import java.util.logging.Logger;

public class SaleMemoryRepository extends MemoryRepository<Long,Sale> implements SaleRepository {
}