package demo.pos.business;

import demo.persistence.Repository;
import demo.persistence.memory.MemoryRepository;
import demo.pos.domain.sale.Sale;
import demo.pos.persistence.*;


/**
 * Created by overvelj on 14/11/2016.
 */
public class SaleService {
    private final SaleRepository salesRepository ;

    public SaleService(SaleRepository saleRepository) {
        this.salesRepository=saleRepository;
    }


    public void addSale(Sale sale){
        salesRepository.insert(sale.getId(),sale);
    }

    public long countSales(){
        return salesRepository.count();
    }

}
