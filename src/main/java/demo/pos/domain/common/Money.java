package demo.pos.domain.common;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;


/**
 * @author Jan de Rijke.
 * We'd really need Joda currency, but we do not want to depend on a library
 */
public class Money {
	double amount;
	Locale locale; // the currency is the currency of this local's country
	NumberFormat format;

	public Money(double amount, Locale locale) {
		this.amount = amount;
		this.locale = locale;
		format = NumberFormat.getCurrencyInstance(locale);

	}

	public Money(double amount) {
		this(amount,Locale.getDefault());
	}

	public Money(double price, String local) {
		this(price,new Locale(local));
	}

	@Override
	public String toString() {
		return format.format(amount);
	}

	public double getAmount() {
		return amount;
	}

	public Money calculate(DoubleUnaryOperator operator){
		return new Money (operator.applyAsDouble(amount),locale);
	}

	// Do a binary Money operation using this as the first argument and operand as the seoond
	// The result is in the Locale of this
	public Money calculate(DoubleBinaryOperator operator, Money operand){
		return new Money (operator.applyAsDouble(amount,convertToAmount(operand,locale)),locale);
	}

	// conversion from operand.locale to locale not implemented
	// lacking up tu date conversion rates
	private double convertToAmount(Money operand, Locale locale) {
		return operand.amount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Money money = (Money) o;
		return Double.compare(money.amount, amount) == 0 &&
			locale.equals(money.locale);
	}

	public boolean lowerThenOrEqual(Money other){
		return getAmount() <= other.getAmount() + 0.001;
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, locale);
	}

	public Locale getLocale() {
		return locale;
	}
}