package demo.pos.domain.sale;

import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;

/**
 * Created by overvelj on 14/11/2016.
 */
public class SalesLineItem {
		private  long id;
    private ProductDescription pd;
    private int qty;

    public ProductDescription getPd() {
        return pd;
    }


    public int getQty() {
        return qty;
    }

    // unmodifiablev aggregate member
    public SalesLineItem(long nextLineItemId, ProductDescription pd, int qty) {

        this.pd = pd;
        this.qty = qty;
    }

    public Money getSubTotal() {

        return pd.getPrice().calculate(m -> m*qty);
    }
}
