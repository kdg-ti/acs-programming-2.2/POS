package demo.pos.application;

import demo.pos.business.ProductService;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;
import demo.pos.domain.sale.Sale;
import demo.pos.business.SaleService;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Register {
    private Sale s;
    private ProductService productService;
    private SaleService store;

    public ProductService getProductService() {
        return productService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }


    public Sale getSale() {
        return s;
    }

    public void enterItem(int id, int qty) {
        ProductDescription itemDesc = productService.getProductDesc(id);
        s.makeSalesLineItem(itemDesc, qty);
    }

    public SaleService getStore() {
        return store;
    }

    public Register(ProductService catalog, SaleService store) {
        this.productService = catalog;
        this.store = store;
    }

    public void endSale() {
        s.setComplete();
    }

    public Money getTotal() {
        return s.getTotal();
    }

    public void makePayment(Money i) {
        s.makePayment(i);
        store.addSale(s);
    }

    public void makeNewSale() {
        s = new Sale();
    }
}
