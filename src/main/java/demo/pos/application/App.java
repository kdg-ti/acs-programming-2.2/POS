package demo.pos.application;

import demo.pos.Pos;
import demo.pos.business.ProductService;
import demo.pos.domain.common.Money;
import demo.pos.domain.product.ProductDescription;

/**
 * @author Jan de Rijke.
 */
public class App {
	public static void main(String[] args) {
		Pos app=new Pos();
		app.init();
		Register r = app.getRegister();
		ProductService catalog = r.getProductService();
		ProductDescription pd1 = new ProductDescription(1, "Een", new Money(1.2));
		ProductDescription pd2 = new ProductDescription(2, "Twee", new Money(3.54));
		catalog.addProduct(pd1);
		catalog.addProduct(pd2);
		app.close();
	}
}