package demo.persistence.memory;

import demo.persistence.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Jan de Rijke.
 */
public class MemoryRepository<K,V> implements Repository<K, V> {
	private static Logger log=  Logger.getLogger("demo.persistence.memory");
	protected Map<K, V> data =new ConcurrentHashMap<>();

	public MemoryRepository() {
		log.info("new memory repository " + getClass());
	}

	@Override
	public boolean update(K key, V value) {
		return  data.put(key, value)!=null;
	}

	@Override
	public V insert(K key, V value) {
		update(key,value);
		return value;
	}

	@Override
	public V findById(K id) {
		return data.get(id);
	}

	@Override
	public long count() {
		return data.size();
	}

	public List<V> findBy(Predicate<V> predicate) {
		return findStreamBy(predicate).collect(Collectors.toList());
	}

	public List<V> findByAndOrder(Predicate<V> predicate, Comparator<V> sorter) {
		Stream<V> result = findStreamBy(predicate);
		if (sorter != null) {
			result = result.sorted(sorter);
		}
		return result.collect(Collectors.toList());
	}

	public Optional<V> findOneBy(Predicate<V> predicate) {
		return findStreamBy(predicate).findAny();
	}

	private Stream<V> findStreamBy(Predicate<V> predicate) {
		return data.values().stream().filter(predicate);
	}



}
