package demo.persistence.db;

import java.sql.*;

/**
 * @author Jan de Rijke.
 */
public class DbConnection {


	private static  String dbUrl;
	private static Connection cx;

	private DbConnection() {
	}

	synchronized
	public static void init(String url){
		dbUrl=url;
		cx=null;

	}

	synchronized
	public static Connection getInstance() {
		try {
			if (cx == null) {
				cx = DriverManager.getConnection(dbUrl);
			}
		} catch(SQLNonTransientConnectionException e){
			// ignore derby drop exception
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return cx;
	}

	synchronized
	public static void  close(){
		if (cx != null){
			try {
				cx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		cx=null;
	}
}